import { TenantUserService, UserService } from '@castlecraft/auth';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventPayload } from '../../interfaces/event-payload.interface';

export const UserAccountAddedEvent = 'UserAccountAddedEvent';
export const UserAccountModifiedEvent = 'UserAccountModifiedEvent';
export const UserAccountRemovedEvent = 'UserAccountRemovedEvent';
export const PhoneVerifiedEvent = 'PhoneVerifiedEvent';
export const EmailVerifiedAndUpdatedEvent = 'EmailVerifiedAndUpdatedEvent';
export const EmailVerifiedAndPasswordSetEvent =
  'EmailVerifiedAndPasswordSetEvent';

@Controller('users')
export class UsersController {
  constructor(
    private readonly user: UserService,
    private readonly tenantUser: TenantUserService,
  ) {}

  @EventPattern(UserAccountAddedEvent)
  userAccountAdded(payload: EventPayload) {
    this.user
      .findOne({ uuid: payload?.eventData?.user?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.user);
        }
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(UserAccountModifiedEvent)
  userAccountModified(payload: EventPayload) {
    return this.user
      .updateOne(
        { uuid: payload?.eventData?.user?.uuid },
        { $set: payload?.eventData?.user },
      )
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(UserAccountRemovedEvent)
  userAccountRemoved(payload: EventPayload) {
    this.user
      .deleteOne({ uuid: payload?.eventData?.deletedUser?.uuid })
      .then(userRemoved =>
        this.tenantUser.deleteMany({
          user: payload?.eventData?.deletedUser?.uuid,
        }),
      )
      .then(tenantUserRemoved => {})
      .catch(err => {});
  }

  @EventPattern(PhoneVerifiedEvent)
  phoneVerified(payload: EventPayload) {
    return this.user
      .updateOne(
        { uuid: payload?.eventData?.user?.uuid },
        { $set: payload?.eventData?.user },
      )
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(EmailVerifiedAndUpdatedEvent)
  emailVerifiedAndUpdated(payload: EventPayload) {
    return this.user
      .updateOne(
        { uuid: payload?.eventData?.user?.uuid },
        { $set: payload?.eventData?.user },
      )
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(EmailVerifiedAndPasswordSetEvent)
  emailVerifiedAndPasswordSet(payload: EventPayload) {
    return this.user
      .updateOne(
        { uuid: payload?.eventData?.user?.uuid },
        { $set: payload?.eventData?.user },
      )
      .then(savedUser => {})
      .catch(err => {});
  }
}
