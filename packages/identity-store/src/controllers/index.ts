import { ClientsController } from './clients/clients.controller';
import { TokenController } from './token/token.controller';
import { UsersController } from './users/users.controller';

export const Controllers = [
  ClientsController,
  TokenController,
  UsersController,
];
