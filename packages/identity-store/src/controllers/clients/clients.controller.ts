import { ClientService } from '@castlecraft/auth';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventPayload } from '../../interfaces';

export const ClientAddedEvent = 'ClientAddedEvent';
export const ClientModifiedEvent = 'ClientModifiedEvent';
export const ClientSecretVerifiedEvent = 'ClientSecretVerifiedEvent';
export const OAuth2ClientRemovedEvent = 'OAuth2ClientRemovedEvent';

@Controller('clients')
export class ClientsController {
  constructor(private readonly client: ClientService) {}

  @EventPattern(ClientAddedEvent)
  clientAdded(payload: EventPayload) {
    if (payload?.eventData?.client) {
      this.client
        .insertOne(payload?.eventData?.client)
        .then(added => {})
        .catch(err => {});
    }
  }

  @EventPattern(ClientModifiedEvent)
  clientModified(payload: EventPayload) {
    if (payload?.eventData?.client) {
      this.client
        .updateOne(
          { clientId: payload?.eventData?.client?.clientId },
          { $set: payload?.eventData?.client },
        )
        .then(added => {})
        .catch(err => {});
    }
  }

  @EventPattern(ClientSecretVerifiedEvent)
  clientSecretVerified(payload: EventPayload) {
    if (payload?.eventData?.client) {
      this.client
        .updateOne(
          { clientId: payload?.eventData?.client?.clientId },
          { $set: payload?.eventData?.client },
        )
        .then(added => {})
        .catch(err => {});
    }
  }

  @EventPattern(OAuth2ClientRemovedEvent)
  clientRemoved(payload: EventPayload) {
    if (payload?.eventData?.client) {
      this.client
        .deleteOne({ clientId: payload?.eventData?.client?.clientId })
        .then(added => {})
        .catch(err => {});
    }
  }
}
