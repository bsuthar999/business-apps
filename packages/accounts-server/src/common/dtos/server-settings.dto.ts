import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsUrl, IsUUID } from 'class-validator';

export class ServerSettingsDto {
  @ApiProperty()
  @IsUrl()
  appURL?: string;
  @ApiProperty()
  @IsUrl()
  authServerURL?: string;
  @ApiProperty()
  @IsNotEmpty()
  clientId?: string;
  @ApiProperty()
  @IsNotEmpty()
  clientSecret?: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  profileURL?: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  tokenURL?: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  introspectionURL?: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  authorizationURL?: string;
  @ApiProperty({ isArray: true })
  @IsUrl({ allow_underscores: true }, { each: true })
  @IsOptional()
  callbackURLs?: string[];
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  revocationURL?: string;
  @ApiProperty()
  @IsUUID()
  @IsOptional()
  clientTokenUuid?: string;
  @ApiProperty()
  @IsOptional()
  callbackProtocol?: string;
}
