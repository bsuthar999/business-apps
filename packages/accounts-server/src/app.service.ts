import { Injectable } from '@nestjs/common';
import { SERVICE } from './constants/app-strings';
import { RootApiResponse } from './common/dtos/root-api.response';

@Injectable()
export class AppService {
  getRoot() {
    return { service: SERVICE } as RootApiResponse;
  }
}
