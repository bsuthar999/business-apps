import { Injectable, NotFoundException } from '@nestjs/common';
import { of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { AggregateRoot } from '@nestjs/cqrs';
import { AccountDto } from '../../entities/account/account-dto';
import { AccountService } from '../../entities/account/account.service';
import { Account } from '../../entities/account/account-entity';
import { AccountAddedEvent } from '../../events/account-added/account-added.event';
import { AccountRemovedEvent } from '../../events/account-removed/account-removed.event';
import { AccountUpdatedEvent } from '../../events/account-updated/account-updated.event';

@Injectable()
export class AccountsAggregateService extends AggregateRoot {
  constructor(private readonly accountService: AccountService) {
    super();
  }

  async addAccount(accountPayload: AccountDto) {
    const account = new Account();
    Object.assign(account, accountPayload);
    account.uuid = uuidv4();
    account.tenantId = accountPayload.tenantId;
    account.children = accountPayload.children;
    this.apply(new AccountAddedEvent(account));
    return of(account);
  }

  async getAccountsList(offset, limit, sort, filter_query) {
    return await this.accountService.list(
      offset || 0,
      limit || 10,
      sort,
      filter_query,
    );
  }

  async removeAccount(uuid: string) {
    const account = await this.accountService.findOne({ uuid });
    if (!account) {
      throw new NotFoundException();
    }
    this.apply(new AccountRemovedEvent(account));
  }

  async updateAccount(updatePayload: AccountDto) {
    const provider = await this.accountService.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new AccountUpdatedEvent(updatePayload));
  }
}
