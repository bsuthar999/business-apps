import { AccountAddedHandler } from './account-added/account-added.handler';
import { AccountRemovedHandler } from './account-removed/account-removed.handler';
import { AccountUpdatedHandler } from './account-updated/account-updated.handler';

export const AccountEventManager = [
  AccountAddedHandler,
  AccountRemovedHandler,
  AccountUpdatedHandler,
];
