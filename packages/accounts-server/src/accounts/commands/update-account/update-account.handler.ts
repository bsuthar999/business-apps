import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { UpdateAccountCommand } from './update-account.command';

@CommandHandler(UpdateAccountCommand)
export class UpdateAccountHandler
  implements ICommandHandler<UpdateAccountCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: AccountsAggregateService,
  ) {}

  async execute(command: UpdateAccountCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateAccount(updatePayload);
    aggregate.commit();
    return updatePayload;
  }
}
