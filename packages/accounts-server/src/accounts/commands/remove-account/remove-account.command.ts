import { ICommand } from '@nestjs/cqrs';

export class RemoveAccountCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
