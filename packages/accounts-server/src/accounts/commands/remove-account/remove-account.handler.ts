import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { RemoveAccountCommand } from './remove-account.command';

@CommandHandler(RemoveAccountCommand)
export class RemoveAccountHandler
  implements ICommandHandler<RemoveAccountCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AccountsAggregateService,
  ) {}
  async execute(command: RemoveAccountCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeAccount(uuid);
    aggregate.commit();
  }
}
