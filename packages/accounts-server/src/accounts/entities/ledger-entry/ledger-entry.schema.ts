import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
    postingDate: { type: Date },
    transactionDate: { type: Date, default: () => new Date() },
    account: { type: String },
    debitAmount: { type: Number },
    creditAmount: { type: Number },
    accountCurrency: { type: Number },
    debitInAccountCurrency: { type: Number },
    creditInAccountCurrency: { type: Number },
    journalEntry: { type: String },
    remarks: { type: String },
    company: { type: String },
  },
  { collection: 'ledger_entries', versionKey: false },
);

export const LedgerEntry = schema;

export const LEDGER_ENTRY = 'LedgerEntry';
