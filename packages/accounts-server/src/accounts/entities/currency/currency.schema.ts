import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    name: { type: String },
    symbol: { type: String, unique: true, sparse: true },
    code: { type: String, index: true },
    country: { type: String },
    fraction: { type: Number },
    fractionUnits: { type: Number },
    smallestFraction: { type: Number },
    numberFormat: { type: String },
  },
  { collection: 'currencies', versionKey: false },
);

export const Currency = schema;

export const CURRENCY = 'Currency';
