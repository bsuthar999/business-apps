import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
    children: { type: [String], index: true },
  },
  { collection: 'companies', versionKey: false },
);

export const Company = schema;

export const COMPANY = 'Company';
