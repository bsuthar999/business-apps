import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
  },
  { collection: 'journal_entries', versionKey: false },
);

export const JournalEntry = schema;

export const JOURNAL_ENTRY = 'JournalEntry';
