import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { DATABASE_CONNECTION } from '../../database.provider';
import { Account, ACCOUNT } from './account/account.schema';
import { AccountService } from './account/account.service';
import { Company, COMPANY } from './company/company.schema';
import { CompanyService } from './company/company.service';
import { CURRENCY, Currency } from './currency/currency.schema';
import { CurrencyService } from './currency/currency.service';
import {
  JournalEntry,
  JOURNAL_ENTRY,
} from './journal-entry/journal-entry.schema';
import { JournalEntryService } from './journal-entry/journal-entry.service';
import { LedgerEntry, LEDGER_ENTRY } from './ledger-entry/ledger-entry.schema';
import { LedgerEntryService } from './ledger-entry/ledger-entry.service';

export const AccountEntities: Provider[] = [
  {
    provide: ACCOUNT,
    useFactory: (connection: Connection) => connection.model(ACCOUNT, Account),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: COMPANY,
    useFactory: (connection: Connection) => connection.model(COMPANY, Company),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: CURRENCY,
    useFactory: (connection: Connection) =>
      connection.model(CURRENCY, Currency),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: JOURNAL_ENTRY,
    useFactory: (connection: Connection) =>
      connection.model(JOURNAL_ENTRY, JournalEntry),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: LEDGER_ENTRY,
    useFactory: (connection: Connection) =>
      connection.model(LEDGER_ENTRY, LedgerEntry),
    inject: [DATABASE_CONNECTION],
  },
];

export const AccountEntityServices: Provider[] = [
  AccountService,
  CompanyService,
  CurrencyService,
  JournalEntryService,
  LedgerEntryService,
];
