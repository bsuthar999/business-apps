import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
    children: { type: [String], index: true },
    company: { type: String, index: true },
    currency: { type: String, index: true },
    parentAccount: { type: String, index: true },
    accountName: { type: String, index: true },
    accountNumber: { type: String, index: true },
  },
  { collection: 'accounts', versionKey: false },
);

export const Account = schema;

export const ACCOUNT = 'Account';
