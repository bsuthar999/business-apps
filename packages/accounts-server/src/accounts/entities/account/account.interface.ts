import { Document } from 'mongoose';

export interface Account extends Document {
  uuid: string;
  tenantId: string;
  children: string[];
  company: string;
  currency: string;
  parentAccount: string;
  accountName: string;
  accountNumber: string;
}
