import { Injectable, Logger } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as Joi from 'joi';

export interface EnvConfig {
  [prop: string]: string;
}

export const NODE_ENV = 'NODE_ENV';
export const ID_DB_NAME = 'ID_DB_NAME';
export const ID_DB_HOST = 'ID_DB_HOST';
export const ID_DB_USER = 'ID_DB_USER';
export const ID_DB_PASSWORD = 'ID_DB_PASSWORD';
export const EVENTS_DB_NAME = 'EVENTS_DB_NAME';
export const EVENTS_DB_HOST = 'EVENTS_DB_HOST';
export const EVENTS_DB_USER = 'EVENTS_DB_USER';
export const EVENTS_DB_PASSWORD = 'EVENTS_DB_PASSWORD';
export const MONGO_URI_PREFIX = 'MONGO_URI_PREFIX';
export const EVENTS_HOST = 'EVENTS_HOST';
export const EVENTS_PROTO = 'EVENTS_PROTO';
export const EVENTS_PORT = 'EVENTS_PORT';
export const EVENTS_USER = 'EVENTS_USER';
export const EVENTS_PASSWORD = 'EVENTS_PASSWORD';

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.config().parsed;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      [NODE_ENV]: Joi.string()
        .valid('development', 'production', 'test', 'provision', 'staging')
        .default('development'),
      [ID_DB_NAME]: Joi.string().required(),
      [ID_DB_HOST]: Joi.string().required(),
      [ID_DB_USER]: Joi.string().required(),
      [ID_DB_PASSWORD]: Joi.string().required(),
      [EVENTS_DB_NAME]: Joi.string().required(),
      [EVENTS_DB_HOST]: Joi.string().required(),
      [EVENTS_DB_USER]: Joi.string().required(),
      [EVENTS_DB_PASSWORD]: Joi.string().required(),
      [MONGO_URI_PREFIX]: Joi.string().optional(),
      [EVENTS_PROTO]: Joi.string().optional(),
      [EVENTS_USER]: Joi.string().optional(),
      [EVENTS_PASSWORD]: Joi.string().optional(),
      [EVENTS_HOST]: Joi.string().optional(),
      [EVENTS_PORT]: Joi.string().optional(),
    });

    const { error, value: validatedEnvConfig } =
      envVarsSchema.validate(envConfig);
    if (error) {
      Logger.error(error, undefined, this.constructor.name);
      process.exit(1);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
