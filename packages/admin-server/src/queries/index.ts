import { FetchErrorLogHandler } from './fetch-error-log/fetch-error-log.handler';
import { FetchEventHandler } from './fetch-event/fetch-event.handler';
import { ListErrorLogHandler } from './list-error-logs/list-error-logs.handler';
import { ListEventHandler } from './list-events/list-events.handler';
import { ListTenantRoleHandler } from './list-tenant-role/list-tenant-role.handler';
import { ListTenantUserHandler } from './list-tenant-user/list-tenant-user.handler';
import { ListTenantHandler } from './list-tenant/list-tenant.handler';

export const AdminQueryHandlers = [
  FetchErrorLogHandler,
  FetchEventHandler,
  ListErrorLogHandler,
  ListEventHandler,
  ListTenantRoleHandler,
  ListTenantUserHandler,
  ListTenantHandler,
];
