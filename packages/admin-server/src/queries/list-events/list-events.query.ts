import { CreatedByActor } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';

export class ListEventsQuery implements IQuery {
  constructor(
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
    public readonly eventId: string,
    public readonly eventName: string,
    public readonly eventFromService: string,
    public readonly eventFromDateTime: Date,
    public readonly eventToDataTime: Date,
    public readonly offset: number = 0,
    public readonly limit: number = 10,
  ) {}
}
