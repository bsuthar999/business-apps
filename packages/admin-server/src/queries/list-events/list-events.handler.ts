import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { EventAggregateService } from '../../aggregates/event-aggregate/event-aggregate.service';
import { ListEventsQuery } from './list-events.query';

@QueryHandler(ListEventsQuery)
export class ListEventHandler implements IQueryHandler<ListEventsQuery> {
  constructor(private readonly manager: EventAggregateService) {}
  async execute(query: ListEventsQuery) {
    return await this.manager.list(
      query.actorType,
      query.actorUuid,
      query.eventId,
      query.eventName,
      query.eventFromService,
      query.eventFromDateTime,
      query.eventToDataTime,
      query.offset,
      query.limit,
    );
  }
}
