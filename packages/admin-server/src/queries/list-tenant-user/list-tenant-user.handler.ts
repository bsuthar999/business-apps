import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { ListTenantUserQuery } from './list-tenant-user.query';

@QueryHandler(ListTenantUserQuery)
export class ListTenantUserHandler
  implements IQueryHandler<ListTenantUserQuery>
{
  constructor(private readonly manager: TenantAggregateService) {}
  async execute(query: ListTenantUserQuery) {
    return await this.manager.listTenants(
      query.actorType,
      query.actorUuid,
      query.tenantId,
      query.offset,
      query.limit,
    );
  }
}
