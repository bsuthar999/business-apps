import { CreatedByActor } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';

export class ListTenantRoleQuery implements IQuery {
  constructor(
    public readonly roleName: string,
    public readonly offset: number,
    public readonly limit: number,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
