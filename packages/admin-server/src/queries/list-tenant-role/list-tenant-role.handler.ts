import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { ListTenantRoleQuery } from './list-tenant-role.query';

@QueryHandler(ListTenantRoleQuery)
export class ListTenantRoleHandler
  implements IQueryHandler<ListTenantRoleQuery>
{
  constructor(private readonly manager: TenantAggregateService) {}

  async execute(query: ListTenantRoleQuery) {
    return await this.manager.listTenantRoles(
      query.actorType,
      query.actorUuid,
      query.roleName,
      query.offset,
      query.limit,
    );
  }
}
