import { CreatedByActor } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';

export class ListErrorLogQuery implements IQuery {
  constructor(
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
    public readonly uuid: string,
    public readonly message: string,
    public readonly fromService: string,
    public readonly fromDateTime: Date,
    public readonly toDataTime: Date,
    public readonly offset: number = 0,
    public readonly limit: number = 10,
  ) {}
}
