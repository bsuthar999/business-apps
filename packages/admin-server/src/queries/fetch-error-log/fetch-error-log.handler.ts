import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ErrorLogAggregateService } from '../../aggregates/error-log-aggregate/error-log-aggregate.service';
import { FetchErrorLogQuery } from './fetch-error-log.query';

@QueryHandler(FetchErrorLogQuery)
export class FetchErrorLogHandler implements IQueryHandler<FetchErrorLogQuery> {
  constructor(private readonly event: ErrorLogAggregateService) {}

  async execute(query: FetchErrorLogQuery) {
    return await this.event.fetchErrorLog(
      query.actorType,
      query.actorUuid,
      query.uuid,
    );
  }
}
