import { CreatedByActor } from '@castlecraft/auth';
import { DomainEventService } from '@castlecraft/event-log';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ActorService } from '../../policies/actor/actor.service';

@Injectable()
export class EventAggregateService {
  constructor(
    private readonly domainEvent: DomainEventService,
    private readonly actorPolicy: ActorService,
  ) {}

  async list(
    actorType: CreatedByActor,
    actorUuid: string,
    eventId: string,
    eventName: string,
    eventFromService: string,
    eventFromDateTime: Date,
    eventToDataTime: Date,
    offset: number = 0,
    limit: number = 10,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const query = {
      eventId,
      eventName,
      eventFromService,
      eventDateTime:
        eventFromDateTime && eventToDataTime
          ? {
              $gte: eventFromDateTime,
              $lt: eventToDataTime,
            }
          : undefined,
    };
    const docs = await this.domainEvent.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs,
      length: await this.domainEvent.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async fetchEvent(
    actorType: CreatedByActor,
    actorUuid: string,
    eventId: string,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const event = await this.domainEvent.findOne({ eventId });
    if (!event) {
      throw new NotFoundException({ DomainEventNotFound: eventId });
    }
    return event;
  }
}
