import { Test, TestingModule } from '@nestjs/testing';
import { DomainEventService } from '@castlecraft/event-log';
import { EventAggregateService } from './event-aggregate.service';
import { ActorService } from '../../policies/actor/actor.service';

describe('EventAggregateService', () => {
  let service: EventAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventAggregateService,
        { provide: ActorService, useValue: {} },
        { provide: DomainEventService, useValue: {} },
      ],
    }).compile();

    service = module.get<EventAggregateService>(EventAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
