import {
  CreatedByActor,
  Tenant,
  TenantRole,
  TenantRoleService,
  TenantService,
  TenantUser,
  TenantUserService,
  UserService,
} from '@castlecraft/auth';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { ADMINISTRATOR } from '../../constants/strings';
import { TenantAddedEvent } from '../../events/tenant-added/tenant-added.event';
import { TenantRemovedEvent } from '../../events/tenant-removed/tenant-removed.event';
import { TenantRoleAddedToUserEvent } from '../../events/tenant-role-added-to-user/tenant-role-added-to-user.event';
import { TenantRoleCreatedEvent } from '../../events/tenant-role-created/tenant-role-created.event';
import { TenantRoleDeletedEvent } from '../../events/tenant-role-deleted/tenant-role-deleted.event';
import { TenantRoleRemovedFromUserEvent } from '../../events/tenant-role-removed-from-user/tenant-role-removed-from-user.event';
import { TenantUserAddedEvent } from '../../events/tenant-user-added/tenant-user-added.event';
import { TenantUserRemovedEvent } from '../../events/tenant-user-removed/tenant-user-removed.event';
import { ActorService } from '../../policies/actor/actor.service';

@Injectable()
export class TenantAggregateService extends AggregateRoot {
  constructor(
    private readonly actorPolicy: ActorService,
    private readonly tenant: TenantService,
    private readonly tenantUser: TenantUserService,
    private readonly tenantRole: TenantRoleService,
    private readonly user: UserService,
  ) {
    super();
  }

  async addTenant(actorType: CreatedByActor, actorUuid: string) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateSuperAdmin(actorType, actor);

    const tenant = {} as Tenant;
    tenant.uuid = uuidv4();
    tenant.createdByActor = actorType;
    tenant.createdById = actorUuid;

    this.apply(new TenantAddedEvent(tenant, actor));

    return tenant;
  }

  async removeTenant(
    tenantId: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateSuperAdmin(actorType, actor);

    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }
    const tenantUsers = await this.tenantUser.find({ tenant: tenantId });
    if (tenantUsers?.length) {
      throw new BadRequestException({ UsersExistForTenant: tenantId });
    }

    this.apply(new TenantRemovedEvent(tenant, actor));
  }

  async addTenantUser(
    tenantId: string,
    userUuid: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateAdmin(actorType, tenantId, actor);

    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new NotFoundException({ userUuid });
    }

    const tenantUser = {} as TenantUser;
    tenantUser.tenant = tenantId;
    tenantUser.user = userUuid;

    this.apply(new TenantUserAddedEvent(tenantUser, actor));

    return tenantUser;
  }

  async removeTenantUser(
    tenantId: string,
    userUuid: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateAdmin(actorType, tenantId, actor);

    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new NotFoundException({ userUuid });
    }

    const tenantUser = {} as TenantUser;
    tenantUser.tenant = tenantId;
    tenantUser.user = userUuid;

    this.apply(new TenantUserRemovedEvent(tenantUser, actor));
  }

  async listTenants(
    actorType: CreatedByActor,
    actorUuid: string,
    tenantId?: string,
    offset: number = 0,
    limit: number = 10,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    const { tUser, aUser } = await this.actorPolicy.validateAdmin(
      actorType,
      tenantId,
      actor,
    );

    const query: { uuid?: string } = {};
    if (tenantId) {
      query.uuid = tenantId;
    }

    if (!aUser.roles.includes(ADMINISTRATOR)) {
      query.uuid = tUser.tenant;
    }

    const data = this.tenant.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs: await data.exec(),
      length: await this.tenant.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async listTenantUsers(
    actorType: CreatedByActor,
    actorUuid: string,
    tenantId: string,
    offset: number = 0,
    limit: number = 10,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateAdmin(actorType, tenantId, actor);
    const { tUser, aUser } = await this.actorPolicy.validateAdmin(
      actorType,
      tenantId,
      actor,
    );

    const query: { tenant?: string } = {};
    if (tenantId) {
      query.tenant = tenantId;
    }

    if (!aUser.roles.includes(ADMINISTRATOR)) {
      query.tenant = tUser.tenant;
    }

    const data = this.tenantUser.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs: await data.exec(),
      length: await this.tenantUser.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async listTenantRoles(
    actorType: CreatedByActor,
    actorUuid: string,
    roleName: string,
    offset: number = 0,
    limit: number = 10,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const query: { roleName?: string } = {};
    if (roleName) {
      query.roleName = roleName;
    }
    const data = this.tenantRole.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs: await data.exec(),
      length: await this.tenantRole.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async createTenantRole(
    roleName: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const role = await this.tenantRole.find({ roleName: roleName.trim() });
    if (role?.length) {
      throw new BadRequestException({ RoleNameExists: roleName.trim() });
    }

    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateSuperAdmin(actorType, actor);

    const tenantRole = {
      uuid: uuidv4(),
      roleName: roleName.trim(),
    } as TenantRole;

    this.apply(new TenantRoleCreatedEvent(tenantRole, actor));
  }

  async deleteTenantRole(
    roleName: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateSuperAdmin(actorType, actor);

    const tenantRole = await this.tenantRole.findOne({ roleName });
    if (!tenantRole) {
      throw new NotFoundException({ roleName });
    }
    const tenantUsersWithRole = await this.tenantUser.find({ roles: roleName });
    if (tenantUsersWithRole?.length) {
      throw new BadRequestException({ UsersExistWithTenantRole: roleName });
    }

    this.apply(new TenantRoleDeletedEvent(tenantRole, actor));
  }

  async addTenantRoleToUser(
    tenantId: string,
    userUuid: string,
    roleName: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateAdmin(actorType, tenantId, actor);

    const tenantRole = await this.tenantRole.findOne({ roleName });
    if (!tenantRole) {
      throw new NotFoundException({ roleName });
    }

    const tenantUser = await this.tenantUser.findOne({
      user: userUuid,
      tenant: tenantId,
    });
    if (!tenantUser) {
      throw new NotFoundException({ user: userUuid });
    }
    if (tenantUser?.roles?.includes(roleName)) {
      throw new BadRequestException({ TenantUserAlreadyHasRole: roleName });
    }

    this.apply(new TenantRoleAddedToUserEvent(tenantUser, tenantRole, actor));
    return { tenantUser, tenantRole };
  }

  async removeTenantRoleFromUser(
    tenantId: string,
    userUuid: string,
    roleName: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    await this.actorPolicy.validateAdmin(actorType, tenantId, actor);

    const tenantRole = await this.tenantRole.findOne({ roleName });
    if (!tenantRole) {
      throw new NotFoundException({ roleName });
    }

    const tenantUser = await this.tenantUser.findOne({
      user: userUuid,
      tenant: tenantId,
    });
    if (!tenantRole) {
      throw new NotFoundException({ user: userUuid });
    }

    this.apply(
      new TenantRoleRemovedFromUserEvent(tenantUser, tenantRole, actor),
    );
    return { tenantUser, tenantRole };
  }
}
