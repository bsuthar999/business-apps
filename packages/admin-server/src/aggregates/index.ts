import { ErrorLogAggregateService } from './error-log-aggregate/error-log-aggregate.service';
import { EventAggregateService } from './event-aggregate/event-aggregate.service';
import { TenantAggregateService } from './tenant-aggregate/tenant-aggregate.service';

export const AdminAggregates = [
  EventAggregateService,
  ErrorLogAggregateService,
  TenantAggregateService,
];
