import { ErrorLogService } from '@castlecraft/event-log';
import { Test, TestingModule } from '@nestjs/testing';
import { ActorService } from '../../policies/actor/actor.service';
import { ErrorLogAggregateService } from './error-log-aggregate.service';

describe('ErrorLogAggregateService', () => {
  let service: ErrorLogAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ErrorLogAggregateService,
        { provide: ActorService, useValue: {} },
        { provide: ErrorLogService, useValue: {} },
      ],
    }).compile();

    service = module.get<ErrorLogAggregateService>(ErrorLogAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
