import { TenantUserService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantRoleAddedToUserEvent } from './tenant-role-added-to-user.event';

@EventsHandler(TenantRoleAddedToUserEvent)
export class TenantRoleAddedToUserHandler
  implements IEventHandler<TenantRoleAddedToUserEvent>
{
  constructor(private readonly tenantUser: TenantUserService) {}

  handle(event: TenantRoleAddedToUserEvent) {
    if (event?.tenantRole) {
      this.tenantUser
        .updateOne(
          { user: event?.tenantUser?.user },
          { $push: { roles: event?.tenantRole?.roleName } },
        )
        .then(added => {})
        .catch(err => {});
    }
  }
}
