import { TenantUserService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantRoleRemovedFromUserEvent } from './tenant-role-removed-from-user.event';

@EventsHandler(TenantRoleRemovedFromUserEvent)
export class TenantRoleRemovedFromUserHandler
  implements IEventHandler<TenantRoleRemovedFromUserEvent>
{
  constructor(private readonly tenantUser: TenantUserService) {}

  handle(event: TenantRoleRemovedFromUserEvent) {
    if (event?.tenantUser) {
      this.tenantUser
        .updateOne(
          { user: event?.tenantUser?.user },
          { $pull: { roles: event?.tenantRole?.roleName } },
        )
        .then(added => {})
        .catch(err => {});
    }
  }
}
