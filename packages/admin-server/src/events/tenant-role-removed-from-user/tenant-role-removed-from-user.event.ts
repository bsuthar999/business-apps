import { Client, TenantRole, TenantUser, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';

export class TenantRoleRemovedFromUserEvent implements IEvent {
  constructor(
    public readonly tenantUser: TenantUser,
    public readonly tenantRole: TenantRole,
    public readonly actor: User | Client,
  ) {}
}
