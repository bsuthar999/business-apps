import { TenantAddedHandler } from './tenant-added/tenant-added.handler';
import { TenantRemovedHandler } from './tenant-removed/tenant-removed.handler';
import { TenantRoleAddedToUserHandler } from './tenant-role-added-to-user/tenant-role-added-to-user.handler';
import { TenantRoleCreatedHandler } from './tenant-role-created/tenant-role-created.handler';
import { TenantRoleDeletedHandler } from './tenant-role-deleted/tenant-role-deleted.handler';
import { TenantRoleRemovedFromUserHandler } from './tenant-role-removed-from-user/tenant-role-removed-from-user.handler';
import { TenantUserAddedHandler } from './tenant-user-added/tenant-user-added.handler';
import { TenantUserRemovedHandler } from './tenant-user-removed/tenant-user-removed.handler';

export const AdminEventHandlers = [
  TenantAddedHandler,
  TenantRemovedHandler,
  TenantRoleAddedToUserHandler,
  TenantRoleCreatedHandler,
  TenantRoleDeletedHandler,
  TenantRoleRemovedFromUserHandler,
  TenantUserAddedHandler,
  TenantUserRemovedHandler,
];
