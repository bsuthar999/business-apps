import { AuthModule } from '@castlecraft/auth';
import { EventLogModule } from '@castlecraft/event-log';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { AdminAggregates } from './aggregates';
import { AdminCommandHandlers } from './commands';
import { ConfigModule } from './config/config.module';
import {
  ConfigService,
  EVENTS_DB_HOST,
  EVENTS_DB_NAME,
  EVENTS_DB_PASSWORD,
  EVENTS_DB_USER,
  ID_DB_HOST,
  ID_DB_NAME,
  ID_DB_PASSWORD,
  ID_DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';
import { TenantControllers } from './controllers';
import { AdminEventHandlers } from './events';
import { AdminPolicies } from './policies';
import { AdminQueryHandlers } from './queries';

@Module({
  imports: [
    ConfigModule,
    AuthModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        identityDbHost: config.get(ID_DB_HOST),
        identityDbUser: config.get(ID_DB_USER),
        identityDbPassword: config.get(ID_DB_PASSWORD),
        identityDbName: config.get(ID_DB_NAME),
      }),
      inject: [ConfigService],
    }),
    EventLogModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        eventsDbHost: config.get(EVENTS_DB_HOST),
        eventsDbUser: config.get(EVENTS_DB_USER),
        eventsDbPassword: config.get(EVENTS_DB_PASSWORD),
        eventsDbName: config.get(EVENTS_DB_NAME),
      }),
      inject: [ConfigService],
    }),
    CqrsModule,
  ],
  controllers: [...TenantControllers],
  providers: [
    ...AdminAggregates,
    ...AdminPolicies,
    ...AdminCommandHandlers,
    ...AdminQueryHandlers,
    ...AdminEventHandlers,
  ],
})
export class AppModule {}
