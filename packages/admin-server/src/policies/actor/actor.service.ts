import {
  Client,
  ClientService,
  CreatedByActor,
  TenantUserService,
  User,
  UserService,
} from '@castlecraft/auth';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { ADMINISTRATOR } from '../../constants/strings';

@Injectable()
export class ActorService {
  constructor(
    private readonly client: ClientService,
    private readonly user: UserService,
    private readonly tenantUser: TenantUserService,
  ) {}

  async validateActor(
    actorType: CreatedByActor,
    actorUuid: string,
  ): Promise<User | Client> {
    if (actorType === CreatedByActor.Client) {
      const client = await this.client.findOne({ clientId: actorUuid });
      if (!client) {
        throw new NotFoundException({ clientId: actorUuid });
      }
      if (!client.isTrusted) {
        throw new UnauthorizedException({ UnTrustedClient: actorUuid });
      }
      return client;
    } else if (actorType === CreatedByActor.User) {
      const user = await this.user.findOne({ uuid: actorUuid });
      if (!user) {
        throw new NotFoundException({ user: actorUuid });
      }
      return user;
    }
  }

  async fetchTenantUser(tenant: string, user: User) {
    const tenantUser = await this.tenantUser.findOne({
      user: user.uuid,
      tenant,
    });
    if (!tenantUser) {
      throw new NotFoundException({ TenantUserNotFound: user.uuid });
    }
    return tenantUser;
  }

  async validateAdmin(
    actorType: CreatedByActor,
    tenantId: string,
    actor: User,
  ) {
    if (actorType === CreatedByActor.User) {
      const tUser = await this.fetchTenantUser(tenantId, actor);
      const isAllowed =
        tUser?.roles?.includes(ADMINISTRATOR) ||
        (actor as User).roles.includes(ADMINISTRATOR);

      if (!isAllowed) {
        throw new ForbiddenException({ NotAllowedToAddTenantUser: actor.uuid });
      }
      return { tUser, aUser: actor as User };
    }
  }

  async validateSuperAdmin(actorType: CreatedByActor, actor: User) {
    if (
      actorType === CreatedByActor.User &&
      !actor?.roles?.includes(ADMINISTRATOR)
    ) {
      throw new ForbiddenException({ NotAllowed: actor.uuid });
    }
  }
}
