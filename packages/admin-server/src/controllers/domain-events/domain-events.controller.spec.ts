import {
  ClientService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { DomainEventsController } from './domain-events.controller';

describe('DomainEventsController', () => {
  let controller: DomainEventsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [DomainEventsController],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    controller = module.get<DomainEventsController>(DomainEventsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
