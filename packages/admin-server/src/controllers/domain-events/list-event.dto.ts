import {
  IsDateString,
  IsNumberString,
  IsString,
  IsUUID,
} from 'class-validator';

export class ListEventDto {
  @IsUUID()
  eventId: string;
  @IsString()
  eventName: string;
  @IsString()
  eventFromService: string;
  @IsDateString()
  eventFromDateTime: string;
  @IsDateString()
  eventToDateTime: string;
  @IsNumberString()
  offset: string;
  @IsNumberString()
  limit: string;
}
