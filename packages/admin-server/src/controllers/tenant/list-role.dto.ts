import { IsNumberString, IsUUID } from 'class-validator';

export class ListRoleDto {
  @IsUUID()
  roleName: string;
  @IsNumberString()
  offset: string;
  @IsNumberString()
  limit: string;
}
