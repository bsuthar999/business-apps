import { IsNumberString, IsUUID } from 'class-validator';

export class ListTenantDto {
  @IsUUID()
  tenantId: string;
  @IsNumberString()
  offset: string;
  @IsNumberString()
  limit: string;
}
