import {
  ClientGuard,
  CreatedByActor,
  RoleGuard,
  Roles,
  TokenGuard,
  User,
  UserGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTenantRoleToUserCommand } from '../../commands/add-tenant-role-to-user/add-tenant-role-to-user.command';
import { AddTenantUserCommand } from '../../commands/add-tenant-user/add-tenant-user.command';
import { AddTenantCommand } from '../../commands/add-tenant/add-tenant.command';
import { CreateTenantRoleCommand } from '../../commands/create-tenant-role/create-tenant-role.command';
import { DeleteTenantRoleCommand } from '../../commands/delete-tenant-role/delete-tenant-role.command';
import { RemoveTenantRoleFromUserCommand } from '../../commands/remove-tenant-role-from-user/remove-tenant-role-from-user.command';
import { RemoveTenantUserCommand } from '../../commands/remove-tenant-user/remove-tenant-user.command';
import { RemoveTenantCommand } from '../../commands/remove-tenant/remove-tenant.command';
import { ADMINISTRATOR } from '../../constants/strings';
import { ListTenantRoleQuery } from '../../queries/list-tenant-role/list-tenant-role.query';
import { ListTenantUserQuery } from '../../queries/list-tenant-user/list-tenant-user.query';
import { ListTenantQuery } from '../../queries/list-tenant/list-tenant.query';
import { ListRoleDto } from './list-role.dto';
import { ListTenantDto } from './list-tenant.dto';
import { TenantRoleDto } from './tenant-role.dto';
import { TenantUserRoleDto } from './tenant-user-role.dto';
import { TenantUserDto } from './tenant-user.dto';
import { TenantDto } from './tenant.dto';

@Controller('tenant')
export class TenantController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create_tenant_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  async createTenantByAdmin(@Req() req) {
    return await this.commandBus.execute(
      new AddTenantCommand(CreatedByActor.User, req?.user?.uuid),
    );
  }

  @Post('v1/create_tenant_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  async createTenantByClient(@Req() req) {
    return await this.commandBus.execute(
      new AddTenantCommand(CreatedByActor.Client, req?.client?.clientId),
    );
  }

  @Post('v1/add_tenant_user')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addTenantUserByAdmin(
    @Req() req: unknown & { user: User },
    @Body() body: TenantUserDto,
  ) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/add_tenant_user_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addTenantUserByClient(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Post('v1/remove_tenant_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantByAdmin(@Req() req, @Body() body: TenantDto) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(
        body.tenantId,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/remove_tenant_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantByClient(@Req() req, @Body() body: TenantDto) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(
        body.tenantId,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Post('v1/remove_tenant_user')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantUserByAdmin(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/remove_tenant_user_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantUserByClient(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/list_for_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantsForAdmin(@Req() req, @Query() query: ListTenantDto) {
    return await this.queryBus.execute(
      new ListTenantQuery(
        query.tenantId,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Get('v1/user/list_for_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantUsersForAdmin(@Req() req, @Query() query: ListTenantDto) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(
        query.tenantId,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantsForClient(@Req() req, @Query() query: ListTenantDto) {
    return await this.queryBus.execute(
      new ListTenantQuery(
        query.tenantId,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/user/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantUsersForClient(@Req() req, @Query() query: ListTenantDto) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(
        query.tenantId,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/role/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listRoleForClient(@Req() req, @Query() query: ListRoleDto) {
    return await this.queryBus.execute(
      new ListTenantRoleQuery(
        query.roleName,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/role/list_for_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listRoleForAdmin(@Req() req, @Query() query: ListRoleDto) {
    return await this.queryBus.execute(
      new ListTenantRoleQuery(
        query.roleName,
        Number(query.offset),
        Number(query.limit),
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/role/create_as_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async createRoleForClient(@Req() req, @Body() body: TenantRoleDto) {
    return await this.commandBus.execute(
      new CreateTenantRoleCommand(
        body.roleName,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Post('v1/role/remove_as_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeRoleForClient(@Req() req, @Body() body: TenantRoleDto) {
    return await this.commandBus.execute(
      new DeleteTenantRoleCommand(
        body.roleName,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/role/create_as_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async createRoleForAdmin(@Req() req, @Body() body: TenantRoleDto) {
    return await this.commandBus.execute(
      new CreateTenantRoleCommand(
        body.roleName,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/role/remove_as_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeRoleForAdmin(@Req() req, @Body() body: TenantRoleDto) {
    return await this.commandBus.execute(
      new DeleteTenantRoleCommand(
        body.roleName,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/user/add_role')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addUserRoleAsAdmin(@Req() req, @Body() body: TenantUserRoleDto) {
    return await this.commandBus.execute(
      new AddTenantRoleToUserCommand(
        body.roleName,
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/user/remove_role')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeUserRoleAsAdmin(@Req() req, @Body() body: TenantUserRoleDto) {
    return await this.commandBus.execute(
      new RemoveTenantRoleFromUserCommand(
        body.roleName,
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }
}
