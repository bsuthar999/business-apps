import { IsUUID } from 'class-validator';

export class TenantDto {
  @IsUUID()
  tenantId: string;
}
