import {
  ClientGuard,
  CreatedByActor,
  TokenGuard,
  UserGuard,
} from '@castlecraft/auth';
import {
  Controller,
  Get,
  Param,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { FetchErrorLogQuery } from '../../queries/fetch-error-log/fetch-error-log.query';
import { ListErrorLogQuery } from '../../queries/list-error-logs/list-error-logs.query';
import { ListErrorLogDto } from './list-error-log.dto';

@Controller('error_logs')
export class ErrorLogsController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listErrorLogsForClient(@Req() req, @Query() query: ListErrorLogDto) {
    return await this.queryBus.execute(
      new ListErrorLogQuery(
        CreatedByActor.Client,
        req?.client?.clientId,
        query.uuid,
        query.message,
        query.fromService,
        new Date(query.fromDateTime),
        new Date(query.toDateTime),
        Number(query.offset),
        Number(query.limit),
      ),
    );
  }

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listErrorLogsForAdmin(@Req() req, @Query() query: ListErrorLogDto) {
    return await this.queryBus.execute(
      new ListErrorLogQuery(
        CreatedByActor.User,
        req?.client?.clientId,
        query.uuid,
        query.message,
        query.fromService,
        new Date(query.fromDateTime),
        new Date(query.toDateTime),
        Number(query.offset),
        Number(query.limit),
      ),
    );
  }

  @Get('v1/fetch_for_admin/:uuid')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async fetchErrorLogForAdmin(@Req() req, @Param('uuid') uuid: string) {
    return await this.queryBus.execute(
      new FetchErrorLogQuery(uuid, CreatedByActor.User, req?.user?.uuid),
    );
  }

  @Get('v1/fetch_for_client/:uuid')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async fetchErrorLogForClient(@Req() req, @Param('uuid') uuid: string) {
    return await this.queryBus.execute(
      new FetchErrorLogQuery(
        uuid,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }
}
