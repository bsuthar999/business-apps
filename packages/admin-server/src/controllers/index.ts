import { DomainEventsController } from './domain-events/domain-events.controller';
import { ErrorLogsController } from './error-logs/error-logs.controller';
import { TenantController } from './tenant/tenant.controller';

export const TenantControllers = [
  DomainEventsController,
  ErrorLogsController,
  TenantController,
];
