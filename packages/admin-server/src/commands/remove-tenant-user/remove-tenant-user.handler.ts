import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { RemoveTenantUserCommand } from './remove-tenant-user.command';

@CommandHandler(RemoveTenantUserCommand)
export class RemoveTenantUserHandler
  implements ICommandHandler<RemoveTenantUserCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveTenantUserCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const removedTenantUser = await aggregate.removeTenantUser(
      command.tenantId,
      command.userUuid,
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return removedTenantUser;
  }
}
