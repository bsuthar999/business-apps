import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { AddTenantRoleToUserCommand } from './add-tenant-role-to-user.command';

@CommandHandler(AddTenantRoleToUserCommand)
export class AddTenantRoleToUserHandler
  implements ICommandHandler<AddTenantRoleToUserCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AddTenantRoleToUserCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const tenantUserRole = await aggregate.addTenantRoleToUser(
      command.tenantId,
      command.userUuid,
      command.roleName,
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return tenantUserRole;
  }
}
