import { ICommand } from '@nestjs/cqrs';
import { CreatedByActor } from '@castlecraft/auth';

export class AddTenantCommand implements ICommand {
  constructor(
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
