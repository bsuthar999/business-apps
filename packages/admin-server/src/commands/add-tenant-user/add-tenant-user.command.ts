import { ICommand } from '@nestjs/cqrs';
import { CreatedByActor } from '@castlecraft/auth';

export class AddTenantUserCommand implements ICommand {
  constructor(
    public readonly tenantId: string,
    public readonly userUuid: string,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
