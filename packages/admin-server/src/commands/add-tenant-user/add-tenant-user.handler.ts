import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { AddTenantUserCommand } from './add-tenant-user.command';

@CommandHandler(AddTenantUserCommand)
export class AddTenantUserHandler
  implements ICommandHandler<AddTenantUserCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AddTenantUserCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const tenantUser = await aggregate.addTenantUser(
      command.tenantId,
      command.userUuid,
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return tenantUser;
  }
}
