import { AddTenantRoleToUserHandler } from './add-tenant-role-to-user/add-tenant-role-to-user.handler';
import { AddTenantUserHandler } from './add-tenant-user/add-tenant-user.handler';
import { AddTenantHandler } from './add-tenant/add-tenant.handler';
import { CreateTenantRoleHandler } from './create-tenant-role/create-tenant-role.handler';
import { DeleteTenantRoleHandler } from './delete-tenant-role/delete-tenant-role.handler';
import { RemoveTenantRoleFromUserHandler } from './remove-tenant-role-from-user/remove-tenant-role-from-user.handler';
import { RemoveTenantUserHandler } from './remove-tenant-user/remove-tenant-user.handler';
import { RemoveTenantHandler } from './remove-tenant/remove-tenant.handler';

export const AdminCommandHandlers = [
  AddTenantRoleToUserHandler,
  AddTenantUserHandler,
  AddTenantHandler,
  CreateTenantRoleHandler,
  DeleteTenantRoleHandler,
  RemoveTenantRoleFromUserHandler,
  RemoveTenantUserHandler,
  RemoveTenantHandler,
];
