import { CreatedByActor } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';

export class CreateTenantRoleCommand implements ICommand {
  constructor(
    public readonly roleName: string,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
