import { CreatedByActor } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';

export class RemoveTenantCommand implements ICommand {
  constructor(
    public readonly tenantId: string,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
