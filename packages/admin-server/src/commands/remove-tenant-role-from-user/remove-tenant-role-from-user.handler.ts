import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { RemoveTenantRoleFromUserCommand } from './remove-tenant-role-from-user.command';

@CommandHandler(RemoveTenantRoleFromUserCommand)
export class RemoveTenantRoleFromUserHandler
  implements ICommandHandler<RemoveTenantRoleFromUserCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveTenantRoleFromUserCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const tenantUserRole = await aggregate.removeTenantRoleFromUser(
      command.tenantId,
      command.userUuid,
      command.roleName,
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return tenantUserRole;
  }
}
