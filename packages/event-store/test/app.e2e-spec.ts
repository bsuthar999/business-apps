import { DomainEventService } from '@castlecraft/event-log';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { EventsController } from '../src/controllers/events/events.controller';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      controllers: [EventsController],
      providers: [
        DomainEventService,
        {
          provide: DomainEventService,
          useValue: {
            insertOne: (...args) => Promise.resolve(),
            deleteMany: (...args) => Promise.resolve(),
          },
        },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', done => {
    return request(app.getHttpServer()).get('/').end(done);
  });

  afterAll(async () => {
    await app.close();
  });
});
