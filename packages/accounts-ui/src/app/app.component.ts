import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import { LOGGED_IN } from './auth/token/constants';
import { TokenService } from './auth/token/token.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  loggedIn: boolean;
  cap = App;
  @HostBinding('class') className = '';
  toggleControl = new FormControl(false);

  constructor(
    private breakpointObserver: BreakpointObserver,
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly router: Router,
    private readonly zone: NgZone,
    private readonly overlay: OverlayContainer,
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
    });
    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
        }
      },
      error: error => {},
    });
    this.toggleControl.valueChanges.subscribe(darkMode => {
      const darkClassName = 'darkMode';
      this.className = darkMode ? darkClassName : '';
      if (darkMode) {
        this.overlay.getContainerElement().classList.add(darkClassName);
      } else {
        this.overlay.getContainerElement().classList.remove(darkClassName);
      }
    });
  }

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split(`${environment.callbackProtocol}://`).pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  logIn() {
    this.token.logIn();
  }

  logOut() {
    this.token.logOut();
  }
}
