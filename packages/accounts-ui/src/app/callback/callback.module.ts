import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { CallbackPageRoutingModule } from './callback-routing.module';
import { CallbackPage } from './callback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CallbackPageRoutingModule,
    MaterialModule,
    FlexModule,
  ],
  declarations: [CallbackPage],
})
export class CallbackPageModule {}
