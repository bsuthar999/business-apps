# Business Apps

Monorepo of business apps

## Prerequisites

- NodeJS
- yarn
- lerna
- docker

## Development

```shell
git clone https://gitlab.com/castlecraft/business-apps && cd business-apps
yarn && lerna bootstrap
```

### Start backing service containers

```shell
docker-compose --project-name ba -f docker/building-blocks.yml up -d
```

### Setup database

To add db(s)/user(s) to MongoDB enter the mongodb container

```shell
docker exec -it ba_mongo_1 bash
```

Execute following within the container

```shell
mongo tokendb \
  --host localhost \
  --port 27017 \
  -u root \
  -p changeit \
  --authenticationDatabase admin \
  --eval "db.createUser({user: 'tokenuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'tokendb'}], passwordDigestor: 'server'});"

mongo eventsdb \
  --host localhost \
  --port 27017 \
  -u root \
  -p changeit \
  --authenticationDatabase admin \
  --eval "db.createUser({user: 'eventsuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'eventsdb'}], passwordDigestor: 'server'});"

mongo accountsdb \
  --host localhost \
  --port 27017 \
  -u root \
  -p changeit \
  --authenticationDatabase admin \
  --eval "db.createUser({user: 'accountsuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'accountsdb'}], passwordDigestor: 'server'});"
```

### Starting NestJS apps for development

```shell
cd packages/<app-server>
cp example-env .env
yarn start:debug
```

Note: start `identity-store` first. It will help store tokens

### Run setup for building blocks

```shell
./docker/setup-wizard \
  --admin-name "System Admin" \
  --admin-email admin@example.com \
  --admin-password Secret@1234 \
  --admin-phone +919876543210 \
  --org-name "Example Inc"
```

### Starting Angular apps for development

```shell
cd packages/<app-ui>
cp src/environment/environment.example.ts src/environment/environment.ts
yarn start
```

Note:
- Add OAuth Client for "Business Apps" on http://admin.localhost:4220/client/new
- Mark it as trusted
- Select all scopes
- Add `callbackUrl` from `environment.ts` as redirect url(s).
- Note the Client ID, copy it to `clientId` in `environment.ts`
