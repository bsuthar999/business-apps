#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

// Generate NestJS Command, Query or Event

// Utils
const printHelp = () =>
  console.info('Usage: gen-cqe [command|event|query] [obj-name] [path]');
const toPascal = string =>
  `${string}`
    .replace(new RegExp(/[-_]+/, 'g'), ' ')
    .replace(new RegExp(/[^\w\s]/, 'g'), '')
    .replace(
      new RegExp(/\s+(.)(\w+)/, 'g'),
      ($1, $2, $3) => `${$2.toUpperCase() + $3.toLowerCase()}`,
    )
    .replace(new RegExp(/\s/, 'g'), '')
    .replace(new RegExp(/\w/), s => s.toUpperCase());

const toKebab = str =>
  str
    .split('')
    .map((letter, idx) =>
      letter.toUpperCase() === letter
        ? `${idx !== 0 ? '-' : ''}${letter.toLowerCase()}`
        : letter,
    )
    .join('');

const validateObjectPath = objFullPath => {
  const objDir = objFullPath.split('/').slice(-1).pop();
  const objNamePlural =
    command.toLowerCase() === 'query' ? 'queries' : command.toLowerCase() + 's';
  if (objDir !== objNamePlural) {
    console.log(
      `Invalid Parent Directory ${objDir}, it should be ${objNamePlural}`,
    );
    process.exit(1);
  }
};

const allowedCommands = ['command', 'event', 'query'];

const command = process.argv[2];
const obj = process.argv[3];
const objPath = process.argv[4];

if (!command || !obj || !objPath) {
  printHelp();
  process.exit(1);
}

if (!fs.existsSync(objPath)) {
  console.error('Invalid Path ' + objPath);
  process.exit(1);
}
validateObjectPath(objPath);

const objName = toPascal(obj) + toPascal(command);
const objHandlerName = toPascal(obj) + 'Handler';

if (!allowedCommands.includes(command.toLowerCase())) {
  console.error(
    'Invalid Command ' + command + '\nAllowed Commands are: ' + allowedCommands,
  );
  process.exit(1);
}

console.info(`Generating ${objName}`);

const objClass = `
import { I${toPascal(command)} } from '@nestjs/cqrs';

export class ${objName} implements I${toPascal(command)} {
  constructor(

  ) {}
}
`;

const eventPublisher =
  command.toLowerCase() === 'command' ? ' EventPublisher,' : '';
const handlerName =
  command.toLowerCase() === 'event' ? 'handle' : 'async execute';
const handlerDecorator =
  command.toLowerCase() === 'event'
    ? 'EventsHandler'
    : `${toPascal(command)}Handler`;

const handlerClass = `import { ${handlerDecorator},${eventPublisher} I${toPascal(
  command,
)}Handler } from '@nestjs/cqrs';
import { ${objName} } from './${toKebab(
  toPascal(obj),
)}.${command.toLowerCase()}';

@${handlerDecorator}(${objName})
export class ${objHandlerName} implements I${toPascal(
  command,
)}Handler<${objName}> {
  constructor(
    ${
      eventPublisher
        ? `private readonly publisher: EventPublisher,
    private readonly manager: Aggregate,`
        : ''
    }
  ) {}

  ${handlerName}(${command.toLowerCase()}: ${objName}) {
    ${
      eventPublisher
        ? 'const aggregate = this.publisher.mergeObjectContext(this.manager);'
        : ''
    }
    ${eventPublisher ? 'aggregate.commit();' : ''}
  }
}
`;

const objDir = path.join(objPath, toKebab(toPascal(obj)));
const objFile = path.join(
  objDir,
  toKebab(toPascal(obj)) + `.${command.toLowerCase()}.ts`,
);
const objHandlerFile = path.join(
  objDir,
  toKebab(toPascal(obj)) + `.handler.ts`,
);

if (!fs.existsSync(objDir)) {
  console.info(`Creating directory ${objDir}`);
  fs.mkdirSync(objDir);
}

if (!fs.existsSync(objFile)) {
  console.info(`Generating ${objFile}`);
  fs.writeFileSync(objFile, objClass);
}

if (!fs.existsSync(objHandlerFile)) {
  console.info(`Generating ${objHandlerFile}`);
  fs.writeFileSync(objHandlerFile, handlerClass);
}
