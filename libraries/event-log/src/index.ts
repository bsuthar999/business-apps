export * from './event-log.module';
export * from './common';
export * from './entities/domain-event/domain-event.interface';
export * from './entities/error-log/error-log.interface';

export { DomainEventService } from './entities/domain-event/domain-event.service';
export { ErrorLogService } from './entities/error-log/error-log.service';
