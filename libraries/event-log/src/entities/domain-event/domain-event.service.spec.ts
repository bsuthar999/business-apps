import { Test, TestingModule } from '@nestjs/testing';
import { DOMAIN_EVENT } from './domain-event.schema';
import { DomainEventService } from './domain-event.service';

describe('DomainEventService', () => {
  let service: DomainEventService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DomainEventService,
        {
          provide: DOMAIN_EVENT,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<DomainEventService>(DomainEventService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
