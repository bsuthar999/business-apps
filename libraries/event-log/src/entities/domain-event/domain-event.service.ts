import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { DomainEvent } from './domain-event.interface';
import { DOMAIN_EVENT } from './domain-event.schema';

@Injectable()
export class DomainEventService {
  constructor(
    @Inject(DOMAIN_EVENT) public readonly model: Model<DomainEvent>,
  ) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<DomainEvent>,
    projections?,
    options?: QueryOptions,
  ): Promise<DomainEvent> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<DomainEvent>,
    projections?,
    options?: QueryOptions,
  ): Promise<DomainEvent[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<DomainEvent>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<DomainEvent>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(
    filter: FilterQuery<DomainEvent>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(
    filter: FilterQuery<DomainEvent>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteMany(filter, options);
  }
}
