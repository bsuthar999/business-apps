import { randomBytes } from 'crypto';
import * as mongoose from 'mongoose';

export function randomBytes32() {
  return randomBytes(32).toString('hex');
}

const schema = new mongoose.Schema(
  {
    eventId: { type: String, index: true, unique: true, sparse: true },
    eventName: String,
    eventFromService: String,
    eventDateTime: Date,
    eventData: mongoose.Schema.Types.Mixed,
  },
  { collection: 'domain_events', versionKey: false },
);

export const DomainEvent = schema;

export const DOMAIN_EVENT = 'DomainEvent';
