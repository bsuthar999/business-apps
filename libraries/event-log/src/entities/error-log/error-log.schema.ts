import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: {
      type: String,
      index: true,
      unique: true,
      sparse: true,
      default: () => uuidv4(),
    },
    message: String,
    fromService: String,
    dateTime: Date,
    data: mongoose.Schema.Types.Mixed,
  },
  { collection: 'error_log', versionKey: false },
);

export const ErrorLog = schema;

export const ERROR_LOG = 'ErrorLog';
