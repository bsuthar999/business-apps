import { Observable } from 'rxjs';
export declare const EVENTLOG_CONNECTION = "EVENTLOG_CONNECTION";
export declare const EventsProvider = "EventsProvider";
export declare const EVENTLOG_MODULE_OPTIONS = "EVENTLOG_MODULE_OPTIONS";
export interface EventsDatabaseOptions {
    mongoUriPrefix: string;
    eventsDbHost: string;
    eventsDbUser: string;
    eventsDbPassword: string;
    eventsDbName: string;
}
export interface AsyncEventsDatabaseOptions {
    inject: any[];
    useFactory: (...args: any[]) => EventsDatabaseOptions;
}
export declare function handleRetry(providerName: string, retryAttempts?: number, retryDelay?: number): <T>(source: Observable<T>) => Observable<T>;
