"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERROR_LOG = exports.ErrorLog = void 0;
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const schema = new mongoose.Schema({
    uuid: {
        type: String,
        index: true,
        unique: true,
        sparse: true,
        default: () => uuid_1.v4(),
    },
    message: String,
    fromService: String,
    dateTime: Date,
    data: mongoose.Schema.Types.Mixed,
}, { collection: 'error_log', versionKey: false });
exports.ErrorLog = schema;
exports.ERROR_LOG = 'ErrorLog';
//# sourceMappingURL=error-log.schema.js.map