"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DOMAIN_EVENT = exports.DomainEvent = exports.randomBytes32 = void 0;
const crypto_1 = require("crypto");
const mongoose = require("mongoose");
function randomBytes32() {
    return crypto_1.randomBytes(32).toString('hex');
}
exports.randomBytes32 = randomBytes32;
const schema = new mongoose.Schema({
    eventId: { type: String, index: true, unique: true, sparse: true },
    eventName: String,
    eventFromService: String,
    eventDateTime: Date,
    eventData: mongoose.Schema.Types.Mixed,
}, { collection: 'domain_events', versionKey: false });
exports.DomainEvent = schema;
exports.DOMAIN_EVENT = 'DomainEvent';
//# sourceMappingURL=domain-event.schema.js.map