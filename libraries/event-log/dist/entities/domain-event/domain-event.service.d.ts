import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { DomainEvent } from './domain-event.interface';
export declare class DomainEventService {
    readonly model: Model<DomainEvent>;
    constructor(model: Model<DomainEvent>);
    insertOne(params: any): Promise<DomainEvent>;
    insertMany(params: any, options?: InsertManyOptions): Promise<DomainEvent[]>;
    findOne(filter?: FilterQuery<DomainEvent>, projections?: any, options?: QueryOptions): Promise<DomainEvent>;
    find(filter?: FilterQuery<DomainEvent>, projections?: any, options?: QueryOptions): Promise<DomainEvent[]>;
    updateOne(filter: FilterQuery<DomainEvent>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<DomainEvent>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<DomainEvent>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<DomainEvent>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}
