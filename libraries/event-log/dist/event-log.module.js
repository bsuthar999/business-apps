"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var EventLogModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventLogModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose = require("mongoose");
const rxjs_1 = require("rxjs");
const common_2 = require("./common");
const entities_1 = require("./entities");
let EventLogModule = EventLogModule_1 = class EventLogModule {
    static registerAsync(options) {
        return {
            module: EventLogModule_1,
            providers: [...this.createAsyncProviders(options)],
        };
    }
    static createAsyncProviders(options) {
        const providers = [
            {
                provide: common_2.EVENTLOG_CONNECTION,
                useFactory: async (options) => {
                    const mongoOptions = 'retryWrites=true';
                    return await rxjs_1.lastValueFrom(rxjs_1.defer(() => mongoose.connect(`${options.mongoUriPrefix}://${options.eventsDbUser}:${options.eventsDbPassword}@${options.eventsDbHost.replace(/,\s*$/, '')}/${options.eventsDbName}?${mongoOptions}`, {
                        useNewUrlParser: true,
                        useUnifiedTopology: true,
                        autoReconnect: false,
                        reconnectTries: 0,
                        reconnectInterval: 0,
                        useCreateIndex: true,
                    })).pipe(common_2.handleRetry(common_2.EventsProvider)));
                },
                inject: [common_2.EVENTLOG_MODULE_OPTIONS],
            },
        ];
        return [this.createAsyncOptionsProvider(options), ...providers];
    }
    static createAsyncOptionsProvider(options) {
        return {
            provide: common_2.EVENTLOG_MODULE_OPTIONS,
            useFactory: options.useFactory,
            inject: options.inject,
        };
    }
};
EventLogModule = EventLogModule_1 = __decorate([
    common_1.Module({
        providers: [...entities_1.EventsEntities, ...entities_1.EventsEntityServices],
        exports: [...entities_1.EventsEntityServices],
    })
], EventLogModule);
exports.EventLogModule = EventLogModule;
//# sourceMappingURL=event-log.module.js.map