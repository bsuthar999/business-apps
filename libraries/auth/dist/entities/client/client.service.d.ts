import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { Client } from './client.interface';
export declare class ClientService {
    readonly model: Model<Client>;
    constructor(model: Model<Client>);
    insertOne(params: any): Promise<Client>;
    insertMany(params: any, options?: InsertManyOptions): Promise<Client[]>;
    findOne(filter?: FilterQuery<Client>, projections?: any, options?: QueryOptions): Promise<Client>;
    find(filter?: FilterQuery<Client>, projections?: any, options?: QueryOptions): Promise<Client[]>;
    updateOne(filter: FilterQuery<Client>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<Client>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<Client>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<Client>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}
