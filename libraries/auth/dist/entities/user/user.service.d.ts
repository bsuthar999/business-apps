import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { User } from './user.interface';
export declare class UserService {
    readonly model: Model<User>;
    constructor(model: Model<User>);
    insertOne(params: any): Promise<User>;
    insertMany(params: any, options?: InsertManyOptions): Promise<User[]>;
    findOne(filter?: FilterQuery<User>, projections?: any, options?: QueryOptions): Promise<User>;
    find(filter?: FilterQuery<User>, projections?: any, options?: QueryOptions): Promise<User[]>;
    updateOne(filter: FilterQuery<User>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<User>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<User>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<User>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}
