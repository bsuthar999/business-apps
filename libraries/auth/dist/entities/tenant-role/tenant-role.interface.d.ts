import { Document } from 'mongoose';
export interface TenantRole extends Document {
    uuid?: string;
    roleName: string;
}
