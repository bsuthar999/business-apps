"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleRetry = exports.AUTH_MODULE_OPTIONS = exports.IdentityProvider = exports.IDENTITY_CONNECTION = void 0;
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
exports.IDENTITY_CONNECTION = 'IDENTITY_CONNECTION';
exports.IdentityProvider = 'IdentityProvider';
exports.AUTH_MODULE_OPTIONS = 'AUTH_MODULE_OPTION';
function handleRetry(providerName, retryAttempts = 9, retryDelay = 3000) {
    return (source) => source.pipe(operators_1.retryWhen(e => e.pipe(operators_1.scan((errorCount, error) => {
        common_1.Logger.error(error, `Unable to connect to the database. Retrying (${errorCount + 1})...`, providerName);
        if (errorCount + 1 >= retryAttempts) {
            throw error;
        }
        return errorCount + 1;
    }, 0), operators_1.delay(retryDelay))));
}
exports.handleRetry = handleRetry;
//# sourceMappingURL=common.js.map