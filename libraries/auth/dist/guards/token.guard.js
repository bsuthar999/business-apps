"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenGuard = void 0;
const common_1 = require("@nestjs/common");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const token_cache_service_1 = require("../entities/token-cache/token-cache.service");
let TokenGuard = class TokenGuard {
    constructor(tokenCacheService) {
        this.tokenCacheService = tokenCacheService;
    }
    canActivate(context) {
        const httpContext = context.switchToHttp();
        const req = httpContext.getRequest();
        const accessToken = this.getAccessToken(req);
        return rxjs_1.from(this.tokenCacheService.findOne({ accessToken })).pipe(operators_1.switchMap(cachedToken => {
            if (Math.floor(new Date().getTime() / 1000) < (cachedToken === null || cachedToken === void 0 ? void 0 : cachedToken.expiresIn)) {
                req.token = cachedToken;
                return rxjs_1.of(true);
            }
            return rxjs_1.of(false);
        }));
    }
    getAccessToken(request) {
        if (!request.headers.authorization) {
            if (!request.query.access_token)
                return null;
        }
        return (request.query.access_token ||
            request.headers.authorization.split(' ')[1] ||
            null);
    }
};
TokenGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [token_cache_service_1.TokenCacheService])
], TokenGuard);
exports.TokenGuard = TokenGuard;
//# sourceMappingURL=token.guard.js.map