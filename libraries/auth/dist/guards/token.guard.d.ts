import { CanActivate, ExecutionContext } from '@nestjs/common';
import { TokenCacheService } from '../entities/token-cache/token-cache.service';
export declare class TokenGuard implements CanActivate {
    private readonly tokenCacheService;
    constructor(tokenCacheService: TokenCacheService);
    canActivate(context: ExecutionContext): import("rxjs").Observable<boolean>;
    getAccessToken(request: any): any;
}
