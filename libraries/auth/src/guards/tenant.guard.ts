import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';
import { Request } from 'express';
import { TokenCache } from '../entities/token-cache/token-cache.interface';
import { TenantUserService } from '../entities/tenant-user/tenant-user.service';

@Injectable()
export class TenantGuard implements CanActivate {
  constructor(private readonly tenantUser: TenantUserService) {}

  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req: Request & { token?: TokenCache; tenants?: string[] } =
      httpContext.getRequest();

    return from(this.tenantUser.find({ user: req.token?.user })).pipe(
      map(tenantUsers => {
        if (tenantUsers?.length > 0) {
          req.tenants = tenantUsers.map(user => user.tenant);
        }
        return true;
      }),
    );
  }
}
