import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { from, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TokenCache } from '../entities/token-cache/token-cache.interface';
import { TokenCacheService } from '../entities/token-cache/token-cache.service';
import { User } from '../entities/user/user.interface';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(private readonly tokenCacheService: TokenCacheService) {}

  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req: Request & { token?: TokenCache; user?: User } =
      httpContext.getRequest();
    const accessToken = this.getAccessToken(req);
    return from(this.tokenCacheService.findOne({ accessToken })).pipe(
      switchMap(cachedToken => {
        if (Math.floor(new Date().getTime() / 1000) < cachedToken?.expiresIn) {
          req.token = cachedToken;
          return of(true);
        }
        return of(false);
      }),
    );
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }
}
