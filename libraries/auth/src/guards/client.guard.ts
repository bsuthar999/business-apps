import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { from, map } from 'rxjs';
import { Client } from '../entities/client/client.interface';
import { ClientService } from '../entities/client/client.service';
import { TokenCache } from '../entities/token-cache/token-cache.interface';

@Injectable()
export class ClientGuard implements CanActivate {
  constructor(private readonly client: ClientService) {}

  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req: Request & { token?: TokenCache; client?: Client } =
      httpContext.getRequest();

    return from(this.client.findOne({ clientId: req.token?.client })).pipe(
      map(client => {
        if (client) {
          req.client = client;
          return true;
        }
        return false;
      }),
    );
  }
}
