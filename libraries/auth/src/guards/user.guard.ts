import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { from, map } from 'rxjs';
import { TokenCache } from '../entities/token-cache/token-cache.interface';
import { User } from '../entities/user/user.interface';
import { UserService } from '../entities/user/user.service';

@Injectable()
export class UserGuard implements CanActivate {
  constructor(private readonly user: UserService) {}

  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req: Request & { token?: TokenCache; user?: User } =
      httpContext.getRequest();

    return from(this.user.findOne({ user: req.token?.user })).pipe(
      map(user => {
        if (user) {
          req.user = user;
          return true;
        }
        return false;
      }),
    );
  }
}
