import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, index: true, unique: true, sparse: true },
    creation: { type: Date },
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    disabled: { type: Boolean, default: false },
    name: String,
    phone: { type: String, unique: true, sparse: true, index: true },
    unverifiedPhone: String,
    email: { type: String, unique: true, sparse: true, index: true },
    roles: [String],
    enable2fa: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
    enablePasswordLess: { type: Boolean, default: false },
    isEmailVerified: { type: Boolean, default: false },
  },
  { collection: 'token_user', versionKey: false },
);

export const User = schema;

export const USER = 'User';

export const UserModel = mongoose.model(USER, User);
