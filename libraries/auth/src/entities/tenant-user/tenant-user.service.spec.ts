import { Test, TestingModule } from '@nestjs/testing';
import { TenantUserService } from './tenant-user.service';
import { TENANT_USER } from './tenant-user.schema';

describe('TenantUserService', () => {
  let service: TenantUserService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenantUserService,
        {
          provide: TENANT_USER,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<TenantUserService>(TenantUserService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
