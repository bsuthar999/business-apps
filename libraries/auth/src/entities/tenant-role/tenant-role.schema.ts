import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, index: true, default: uuidv4 },
    roleName: { type: String, index: true, unique: true, sparse: true },
  },
  { collection: 'tenant_role', versionKey: false },
);

export const TenantRole = schema;

export const TENANT_ROLE = 'TenantRole';
