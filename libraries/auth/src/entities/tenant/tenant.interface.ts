import { Document } from 'mongoose';

export interface Tenant extends Document {
  uuid?: string;
  createdById?: string;
  createdByActor?: CreatedByActor;
}

export enum CreatedByActor {
  User = 'User',
  Client = 'Client',
}
